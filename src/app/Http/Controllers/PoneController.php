<?php
namespace Militaruc\Pone\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller as BaseController;

class PoneController extends BaseController
{
    public function index()
    {
        echo 'Hello from P-one Controller@index';
    }

    public function method2()
    {
        echo 'Hello from P-one Controller@method2';
    }

    /*public function add($a, $b){
        $result = $a + $b;
        return view('testpackageviews::add', compact('result'));
    }

    public function subtract($a, $b){
        echo $a - $b;
    }*/
}