<?php
Route::get('p-one', function(){
    echo 'Testing route for package P-one!';
});

Route::get('p-one2', function(){
    echo 'Testing "p-one2" route for package P-one!';
});


Route::get('p-one-index', 'Militaruc\Pone\App\Http\Controllers\PoneController@index');
Route::get('p-one-method2', 'Militaruc\Pone\App\Http\Controllers\PoneController@method2');